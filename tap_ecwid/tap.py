"""Ecwid tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_ecwid.streams import (
    EcwidStream,
    OrdersStream,
    ProductsStream,
    VariationsStream,
    CategoriesStream
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    OrdersStream,
    ProductsStream,
    VariationsStream,
    CategoriesStream
]


class TapEcwid(Tap):
    """Ecwid tap class."""
    name = "tap-ecwid"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "secret_key",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service"
        ),
        th.Property(
            "storeid",
            th.StringType,
            required=True
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://app.ecwid.com/api/v3",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == "__main__":
    TapEcwid.cli()
