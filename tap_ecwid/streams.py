"""Stream type classes for tap-ecwid."""

from array import ArrayType
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_ecwid.client import EcwidStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.

class OrdersStream(EcwidStream):
    """Define custom stream."""
    name = "orders"
    path = "/orders"
    primary_keys = ["id"]
    replication_key = "updateDate"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("subtotal", th.NumberType),
        th.Property("total", th.NumberType),
        th.Property("giftCardRedemption", th.NumberType),
        th.Property("totalBeforeGiftCardRedemption", th.NumberType),
        th.Property("giftCardDoubleSpending", th.BooleanType),
        th.Property("usdTotal", th.NumberType),
        th.Property("tax", th.NumberType),
        th.Property("paymentMethod", th.StringType),
        th.Property("paymentStatus", th.StringType),
        th.Property("fulfillmentStatus", th.StringType),
        th.Property("fulfillmentStatus", th.StringType),
        th.Property("vendorOrderNumber", th.StringType),
        th.Property("orderNumber", th.NumberType),
        th.Property("refererUrl", th.StringType),
        th.Property("globalReferer", th.StringType),
        th.Property("createDate", th.DateTimeType),
        th.Property("updateDate", th.DateTimeType),
        th.Property("createTimestamp", th.NumberType),
        th.Property("updateTimestamp", th.NumberType),
        th.Property("hidden", th.BooleanType),
        th.Property("orderComments", th.StringType),
        th.Property("privateAdminNotes", th.StringType),
        th.Property("email", th.StringType),
        th.Property("ipAddress", th.StringType),
        th.Property("customerId", th.NumberType),
        th.Property("customerGroupId", th.NumberType),
        th.Property("customerGroup", th.StringType),
        th.Property("customerTaxExempt", th.BooleanType),
        th.Property("customerGroup", th.StringType),
        th.Property("customerTaxIdValid", th.BooleanType),
        th.Property("reversedTaxApplied", th.BooleanType),
        th.Property("discount", th.NumberType),
        th.Property("couponDiscount", th.NumberType),
        th.Property("volumeDiscount", th.NumberType),
        th.Property("membershipBasedDiscount", th.NumberType),
        th.Property("totalAndMembershipBasedDiscount", th.NumberType),
        th.Property("customDiscount", th.CustomType({"type": ["array", "string"]})),
        th.Property('discountCoupon',th.ObjectType(
            th.Property("id",th.NumberType),
            th.Property("name",th.StringType),
            th.Property("code",th.StringType),
            th.Property("discountType",th.StringType),
            th.Property("status",th.StringType),
            th.Property("discount",th.NumberType),
            th.Property("launchDate",th.DateTimeType),
            th.Property("usesLimit",th.StringType),
            th.Property("applicationLimit",th.StringType),
            th.Property("creationDate",th.DateTimeType),
            th.Property("updateDate",th.DateTimeType),
            th.Property("orderCount",th.NumberType),
            
        )),
        th.Property('discountInfo',th.ObjectType(
            th.Property("value",th.NumberType),
            th.Property("type",th.StringType),
            th.Property("base",th.StringType),
            th.Property("orderTotal",th.NumberType),
        )),
        th.Property('items',th.ArrayType(
            th.ObjectType(
                th.Property("id",th.NumberType),
                th.Property("productId",th.NumberType),
                th.Property("categoryId",th.NumberType),
                th.Property("price",th.NumberType),
                th.Property("productPrice",th.NumberType),
                th.Property("sku",th.StringType),
                th.Property("quantity",th.NumberType),
                th.Property("shortDescription",th.StringType),
                th.Property("tax",th.NumberType),
                th.Property("shipping",th.NumberType),
                th.Property("quantityInStock",th.NumberType),
                th.Property("name",th.StringType),
                th.Property("isShippingRequired",th.BooleanType),
                th.Property("weight",th.NumberType),
                th.Property("trackQuantity",th.BooleanType),
                th.Property("fixedShippingRateOnly",th.BooleanType),
                th.Property("imageUrl",th.StringType),
                th.Property("smallThumbnailUrl",th.StringType),
                th.Property("hdThumbnailUrl",th.StringType),
                th.Property("fixedShippingRate",th.NumberType),
                th.Property("digital",th.BooleanType),
                th.Property("couponApplied",th.BooleanType),
                th.Property('selectedOptions',th.ArrayType(
                    th.ObjectType(
                        th.Property("name",th.StringType),
                        th.Property("value",th.StringType),
                        th.Property("valuesArray", th.CustomType({"type": ["array", "string"]})),
                        th.Property("selections",th.ArrayType(th.ObjectType(
                        th.Property("selectionTitle",th.StringType),
                        th.Property("selectionModifier",th.NumberType),
                        th.Property("selectionModifierType",th.StringType),
                    ))),
                        th.Property("type",th.StringType),
                    )
                )),
                th.Property('taxes',th.ArrayType(
                    th.ObjectType(
                        th.Property("name",th.StringType),
                        th.Property("value",th.NumberType),
                        th.Property("total",th.NumberType),
                        th.Property("taxOnDiscountedSubtotal",th.NumberType),
                        th.Property("taxOnShipping",th.NumberType),
                        th.Property("includeInPrice",th.BooleanType),
                    )
                )),
                th.Property('dimensions',th.ObjectType(
                    th.Property("length",th.NumberType),
                    th.Property("width",th.NumberType),
                    th.Property("height",th.NumberType),
                )),
                th.Property('couponAmount',th.NumberType),
                th.Property('discounts',th.ArrayType(th.ObjectType(
                    th.Property("discountInfo",th.ObjectType(
                        th.Property("value",th.NumberType),
                        th.Property("type",th.StringType),
                        th.Property("base",th.StringType),
                        th.Property("orderTotal",th.NumberType),
                    )),
                    th.Property('total',th.NumberType),
                ))),
            )
        ))
        
    ).to_dict()
class ProductsStream(EcwidStream):
    """Define custom stream."""
    name = "products"
    path = "/products"
    primary_keys = ["id"]
    replication_key = "updated"
    schema = th.PropertiesList(
        th.Property('id',th.NumberType),
        th.Property("sku",th.StringType),
        th.Property("thumbnailUrl",th.StringType),
        th.Property("quantity",th.NumberType),
        th.Property("unlimited",th.BooleanType),
        th.Property("inStock",th.BooleanType),
        th.Property("name",th.StringType),
        th.Property("nameTranslated", th.CustomType({"type": ["object", "string"]})),
        th.Property("price",th.NumberType),
        th.Property("defaultDisplayedPrice",th.NumberType),
        th.Property("defaultDisplayedPriceFormatted",th.StringType),
        th.Property("defaultDisplayedPriceFormatted",th.StringType),
        th.Property("tax", th.CustomType({"type": ["object", "string"]})),
        th.Property("wholesalePrices", th.ArrayType(
            th.ObjectType(
                th.Property('quantity',th.NumberType),
                th.Property('price',th.NumberType),
            )
        )),
        th.Property("compareToPrice",th.NumberType),
        th.Property("compareToPriceFormatted",th.StringType),
        th.Property("compareToPriceDiscount",th.NumberType),
        th.Property("compareToPriceDiscountFormatted",th.StringType),
        th.Property("compareToPriceDiscountPercent",th.NumberType),
        th.Property("compareToPriceDiscountPercentFormatted",th.StringType),
        th.Property("isShippingRequired",th.BooleanType),
        th.Property("weight",th.NumberType),
        th.Property("url",th.StringType),
        th.Property("created",th.DateTimeType),
        th.Property("updated",th.DateTimeType),
        th.Property("createTimestamp",th.NumberType),
        th.Property("updateTimestamp",th.NumberType),
        th.Property("productClassId",th.NumberType),
        th.Property("enabled",th.BooleanType),
        th.Property("options", th.CustomType({"type": ["array", "string"]})),
        th.Property("warningLimit",th.NumberType),
        th.Property("fixedShippingRateOnly",th.BooleanType),
        th.Property("fixedShippingRate",th.NumberType),
        th.Property('shipping',th.ObjectType(
            th.Property('type',th.StringType),
            th.Property('methodMarkup',th.NumberType),
            th.Property('flatRate',th.NumberType),
            th.Property("disabledMethods", th.CustomType({"type": ["array", "string"]})),
            th.Property("enabledMethods", th.CustomType({"type": ["array", "string"]})),
        )),
        th.Property("defaultCombinationId",th.NumberType),
        th.Property("description",th.StringType),
        th.Property("descriptionTranslated", th.CustomType({"type": ["object", "string"]})),
        th.Property("media",th.ObjectType(
            th.Property('images',th.ArrayType(
                th.ObjectType(
                    th.Property("id",th.StringType),
                    th.Property("isMain",th.BooleanType),
                    th.Property("orderBy",th.NumberType),
                    th.Property("image160pxUrl",th.StringType),
                    th.Property("image400pxUrl",th.StringType),
                    th.Property("image800pxUrl",th.StringType),
                    th.Property("image1500pxUrl",th.StringType),
                    th.Property("imageOriginalUrl",th.StringType),
                )
            ))
        )),
        th.Property("categoryIds", th.CustomType({"type": ["array", "number"]})),
        th.Property("categories",th.ArrayType(th.ObjectType(
            th.Property('id',th.NumberType),
            th.Property('enabled',th.BooleanType),
        ))),
        th.Property("seoTitle",th.StringType),
        th.Property("seoDescription",th.StringType),
        th.Property("defaultCategoryId",th.NumberType),
        th.Property('favorites',th.ObjectType(
            th.Property('count',th.NumberType),
            th.Property('displayedCount',th.StringType),
        )),
        th.Property('attributes',th.ArrayType(
            th.ObjectType(
                th.Property("id",th.NumberType),
                th.Property("name",th.StringType),
                th.Property("value",th.StringType),
                th.Property("type",th.StringType),
                th.Property("show",th.StringType),
            )
        )),
        th.Property("files", th.CustomType({"type": ["array", "string"]})),
        th.Property('relatedProducts',th.ObjectType(
            th.Property("productIds", th.CustomType({"type": ["array", "string"]})),
            th.Property('relatedCategory',th.ObjectType(
                th.Property("enabled",th.BooleanType),
                th.Property("categoryId",th.NumberType),
                th.Property("productCount",th.NumberType),
            ))
        )),
        th.Property("combinations", th.CustomType({"type": ["array", "string"]})),
        th.Property("subscriptionSettings", th.ObjectType(
            th.Property("subscriptionAllowed",th.BooleanType),
            th.Property("oneTimePurchaseAllowed",th.BooleanType),
            th.Property("oneTimePurchasePrice",th.NumberType),
            th.Property("oneTimePurchasePriceFormatted",th.StringType),
            th.Property("oneTimePurchaseMarkup",th.NumberType),
            th.Property("oneTimePurchaseMarkupFormatted",th.StringType),
            th.Property("oneTimePurchaseMarkupPercent",th.NumberType),
            th.Property("oneTimePurchaseMarkupPercentFormatted",th.StringType),
            th.Property("recurringChargeSettings",th.ArrayType(th.ObjectType(
                th.Property("recurringInterval",th.StringType),
                th.Property("recurringIntervalCount",th.NumberType),
                th.Property("subscriptionPriceWithSignUpFee",th.NumberType),
                th.Property("subscriptionPriceWithSignUpFeeFormatted",th.StringType),
                th.Property("signUpFee",th.NumberType),
                th.Property("signUpFeeFormatted",th.StringType),
            ))),
        )),
        th.Property("dimensions", th.CustomType({"type": ["object", "string"]})),
        th.Property("isSampleProduct", th.BooleanType),
    ).to_dict()
    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "product_id": record["id"],
        }

class VariationsStream(EcwidStream):
    """Define custom stream."""
    name = "variations"
    path = "/products/{product_id}/combinations"
    primary_keys = ["id"]
    replication_key = None
    parent_stream_type = ProductsStream
    records_jsonpath = "$[*]"
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("combinationNumber",th.NumberType),
        th.Property("options", th.CustomType({"type": ["array", "string"]})),
        th.Property("sku",th.StringType),
        th.Property("smallThumbnailUrl",th.StringType),
        th.Property("hdThumbnailUrl",th.StringType),
        th.Property("thumbnailUrl",th.StringType),
        th.Property("imageUrl",th.StringType),
        th.Property("originalImageUrl",th.StringType),
        th.Property("quantity",th.NumberType),
        th.Property("unlimited",th.BooleanType),
        th.Property("isShippingRequired",th.BooleanType),
        th.Property("price",th.NumberType),
        th.Property("wholesalePrices",th.ArrayType(
            th.ObjectType(
                th.Property("quantity",th.NumberType),
                th.Property("price",th.NumberType),
            )
        )),
        th.Property("warningLimit",th.NumberType),
        th.Property("compareToPrice",th.NumberType),
        th.Property("attributes",th.ArrayType(
            th.ObjectType(
                th.Property("id",th.NumberType),
                th.Property("name",th.StringType),
                th.Property("value",th.StringType),
                th.Property("show",th.StringType),
                th.Property("type",th.StringType),
            )
        )),
        th.Property("defaultDisplayedPrice",th.NumberType),
        th.Property("defaultDisplayedPriceFormatted",th.StringType),
    ).to_dict()
class CategoriesStream(EcwidStream):
    """Define custom stream."""
    name = "categories"
    path = "/categories"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("id",th.NumberType),
        th.Property("orderBy",th.NumberType),
        th.Property("hdThumbnailUrl",th.StringType),
        th.Property("thumbnailUrl",th.StringType),
        th.Property("imageUrl",th.StringType),
        th.Property("originalImageUrl",th.StringType),
        th.Property("originalImage",th.ObjectType(
            th.Property('url',th.StringType),
            th.Property('width',th.StringType),
            th.Property('height',th.StringType),
        )),
        th.Property("thumbnail",th.ObjectType(
            th.Property('url',th.StringType),
            th.Property('width',th.StringType),
            th.Property('height',th.StringType),
        )),
        th.Property("name",th.StringType),
        th.Property("nameTranslated", th.CustomType({"type": ["object", "string"]})),
        th.Property("productCount",th.NumberType),
        th.Property("enabledProductCount",th.NumberType),
        th.Property("description",th.StringType),
        th.Property("descriptionTranslated", th.CustomType({"type": ["object", "string"]})),
        th.Property("enabled",th.BooleanType),
    ).to_dict()    
